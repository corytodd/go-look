package main

import (
	"fmt"
)

/**
Global connection pool
*/
var DbPool = &ConnectionPoolWrapper{}

type InitFunction func() (interface{}, error)
type ConnectionPoolWrapper struct {
	size int
	conn chan interface{}
}

/**
Call the init function 'size' times. If the init function fails during any call, then
the creation of the pool is considered a failure. We don't return size because a nil
return value indicates 'size' connections were successfully created.

We call the init function 'size' times to make sure each connection shares the same
state. The init function should set defaults such as character encoding, timezone,
anything that needs to be the same in each connection.
*/
func (p *ConnectionPoolWrapper) InitPool(size int, initfn InitFunction) error {
	// Create a buffered channel allowing 'size' senders
	p.conn = make(chan interface{}, size)
	fmt.Println("Initializing database connection pool with...")
	for x := 0; x < size; x++ {
		conn, err := initfn()
		if err != nil {
			return err
		}

		// If the init function succeeded, add the connection to the channel
		p.conn <- conn
		fmt.Printf("%d ...", x)
	}
	///TODO Use portable newlinw
	fmt.Printf("\nCreated %d connections\n", size)
	p.size = size
	return nil
}

/**
Ask for a connection interface from our channel. If there are no
connections available, we block until a connection is ready
*/
func (p *ConnectionPoolWrapper) GetConnection() interface{} {
	fmt.Println("Allocating pool connection")
	return <-p.conn
}

/**
Return a connection we have used to the pool
*/
func (p *ConnectionPoolWrapper) ReleaseConnection(conn interface{}) {
	fmt.Println("Releasing pool connection")
	p.conn <- conn
}
