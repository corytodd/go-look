/* Paste object description and interface functions for its sorter */
package main

type Paste struct {
	ID    int
	Title string
	Body  string
	Date  string
	URID  string
	Lang  string
	Owner string
}

type Pastes []*Paste

func (s Pastes) Len() int      { return len(s) }
func (s Pastes) Swap(i, j int) { s[i], s[j] = s[j], s[i] }

type ByTitle struct{ Pastes }

func (s ByTitle) Less(i, j int) bool { return s.Pastes[i].Title < s.Pastes[j].Title }

type ByOwner struct{ Pastes }

func (s ByOwner) Less(i, j int) bool { return s.Pastes[i].Owner < s.Pastes[j].Owner }

type ByBody struct{ Pastes }

func (s ByBody) Less(i, j int) bool { return s.Pastes[i].Body < s.Pastes[j].Owner }

type ByDate struct{ Pastes }

func (s ByDate) Less(i, j int) bool { return s.Pastes[i].Date < s.Pastes[j].Date }

type ByUrid struct{ Pastes }

func (s ByUrid) Less(i, j int) bool { return s.Pastes[i].URID < s.Pastes[j].URID }

type ByLang struct{ Pastes }

func (s ByLang) Less(i, j int) bool { return s.Pastes[i].Lang < s.Pastes[j].Lang }
