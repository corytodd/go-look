package main

import (
	"errors"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/robfig/cron"
	"html/template"
	"net/http"
	"strings"
	"time"
)

const PORT = ":8090"
const DB_POOL = 10

type Omni struct {
	Muser  string
	Data   Form
	Mpaste *Paste
	Mtable template.HTML
	Err    error
}

type Form struct {
	Query string
	Lang  string
}

var templates = template.Must(template.ParseFiles(
	"templates/go.html",
	"templates/look.html",
	"templates/paste.html",
	"templates/browse.html",
	"templates/404.html",
	"templates/header.tmpl",
	"templates/navbar.tmpl",
	"templates/footer.tmpl",
))

func getHandler(w http.ResponseWriter, r *http.Request) {

	ip := strings.Split(r.RemoteAddr, ":")[0]
	fmt.Printf("Connection from: %s", ip)

	var omni Omni
	tmpl := mux.Vars(r)["page"]
	omni.Mtable = GetRecent()

	switch tmpl {
	case "":
		tmpl = "go"
		fallthrough
	case "go": /* Landing page with search bar */
	case "paste": /* paste.html needs a Recent Pastes Table */
	case "look": /* On GET we display the last known paste... or the first :) */
		fallthrough
	case "browse": /* Same thing for browse if a GET */
		urid := GetLastPaste()
		http.Redirect(w, r, "/look/"+urid, http.StatusFound)
		return /* <- RETURN!! Send back to look URI handler */
	default:
		err := errors.New("Maybe try a nav button? Willy-nilly URLs are the devil.")
		omni.Err = err
		tmpl = "404"
	}

	err := templates.ExecuteTemplate(w, tmpl+".html", omni)
	if err != nil {
		errorHandler(w, r, err)
	}
}

func postHandler(w http.ResponseWriter, r *http.Request) {
	var omni Omni
	tmpl := mux.Vars(r)["page"]
	fmt.Println("Received POST request for " + tmpl)
	omni.Mtable = GetRecent()
	err := r.ParseForm()

	if err != nil {
		fmt.Println("There was a problem parsing the form data")
		errorHandler(w, r, err)
	}

	switch tmpl {
	case "":
		fallthrough
	case "go":
		// Throw 404
	case "look": /* lone Look. Any /look/$uri will go to its own handler */
		fmt.Println("We are POST and look")
		title := r.FormValue("title")
		body := r.FormValue("paste_bin")
		lang := r.FormValue("language")

		dTime := time.Now().Format(time.RFC850)
		d := &Paste{Body: body, Date: dTime, Title: title, Lang: lang}
		fmt.Println("Saving paste date: ", dTime)
		urid := SavePaste(d)
		http.Redirect(w, r, "/look/"+urid, http.StatusFound)
		return /* <- RETURN!! Send back to /look/$uri handler */
	case "paste":
		/* Throw 404 */
	case "browse":
		query := r.FormValue("query")
		omni.Data.Query = query
		fmt.Println("Me browse " + omni.Data.Query)
	default:
		tmpl = "404"
	}

	err = templates.ExecuteTemplate(w, tmpl+".html", omni)
	if err != nil {
		errorHandler(w, r, err)
	}
}

func lookHandler(w http.ResponseWriter, r *http.Request) {
	var omni Omni

	query := mux.Vars(r)["id"]
	omni.Mpaste = GetPaste(query)
	if omni.Mpaste == nil {
		fmt.Println("Something ugly got through")
		return
	}
	omni.Mtable = GetRecent()
	err := templates.ExecuteTemplate(w, "look.html", omni)
	if err != nil {
		errorHandler(w, r, err)
	}
}

/* Handle all errors, 404, etc */
func errorHandler(w http.ResponseWriter, r *http.Request, err error) {
	fmt.Println("Encountered Error: " + err.Error())
	http.Error(w, err.Error(), http.StatusInternalServerError)
}

func main() {

	/**
	Initialize database pool for DB_POOL connection or die
	*/
	err := DbPool.InitPool(DB_POOL, InitPoolConnections)
	if err != nil {
		fmt.Println("Error opening db" + err.Error() + ". Abort!")
		return
	}

	/**
	Start a cron job to check every XX for Pastes to delete
	*/
	c := cron.New()
	c.AddFunc("@every 1m", func() { RemovePastes() })
	//c.Start()

	println("Starting GoLook @ " + time.Now().Format(time.RFC1123))

	/**
	Define all routing handlers via Gorilla mux, (I <3)
	*/
	r := mux.NewRouter()

	r.HandleFunc("/look/{id}", lookHandler)

	r.HandleFunc("/", postHandler).Methods("POST")
	r.HandleFunc("/", getHandler).Methods("GET")

	r.HandleFunc("/{page}", postHandler).Methods("POST")
	r.HandleFunc("/{page}", getHandler).Methods("GET")

	http.HandleFunc("/authorize", HandleAuthorize)
	http.HandleFunc("/oauth2callback", HandleOAuth2Callback)

	http.Handle("/", r)
	http.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("static/"))))
	fmt.Println("Running on " + PORT)
	http.ListenAndServe(PORT, nil)

}
