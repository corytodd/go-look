package main

import (
	"fmt"
	"sort"
	"testing"
	"time"
)

func TestPasteSort(t *testing.T) {

	s := []*Paste{
		{0, "Random", "a", time.Now().Format(time.StampNano), "aYfbd", "Go", "Cory"},
		{1, "Less", "b", time.Now().Format(time.StampNano), "4rfds", "C", "Praxin"},
		{2, "More", "c", time.Now().Format(time.StampNano), "5tged", "Java", "Mendev"},
		{3, "Singleton", "d", time.Now().Format(time.StampNano), "asdcx", "Python", "Arscion"},
		{4, "Masonic", "e", time.Now().Format(time.StampNano), "8ujgr", "HC12", "Nils"},
		{5, "Split", "f", time.Now().Format(time.StampNano), "vbdf3", "C#", "Cory"},
	}

	sort.Sort(ByTitle{s})
	fmt.Println("Pastes by Title:")
	printPastes(s)

	sort.Sort(ByBody{s})
	fmt.Println("\nPastes by Body:")
	printPastes(s)

	sort.Sort(ByDate{s})
	fmt.Println("\nPastes by Date:")
	printPastes(s)

	sort.Sort(ByUrid{s})
	fmt.Println("\nPastes by URID:")
	printPastes(s)

	sort.Sort(ByLang{s})
	fmt.Println("\nPastes by Lang:")
	printPastes(s)

	sort.Sort(ByOwner{s})
	fmt.Println("\nPastes by Owner:")
	printPastes(s)

}

func printPastes(s []*Paste) {
	for _, o := range s {
		fmt.Printf("%+v)\n", o.Title, o.Body, o.Date, o.URID, o.Lang, o.Owner)
	}
}
