Go.Look.Paste.
------------
Another paste bin that like it simple.

Three non-standard libraries are used for this project:

1. Gorilla mux  
go get github.com/gorilla/mux
 
2. Cron by robfig  
go get github.com/robfig/cron

3. Sqlite3 by Maxim Khitrov  
go get code.google.com/p/go-sqlite/go1/sqlite3
  

This toy project was my first attempt at Go and I'm pretty happy with what the language is capable of.