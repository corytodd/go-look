package main

import (
	"html/template"
	"time"
)

type result struct {
	rank    int
	isLang  bool
	isTitle bool
	isOwner bool
}

const CPU_LIMIT = 10

func Search() template.HTML {
	// Create a pool of channels, each match different characteristics
	// Each channel feeds back to a select loop where results are collected
	// Post-process results for relevance
	// End once all channels complete
	// Return table of results

	c := make(chan result, CPU_LIMIT) /* CPU_LIMIT will keep the search process from consuming too much resource */

	for n := 0; n < CPU_LIMIT; n++ {
		go doSearch(c)
	}

	for i := 0; i < CPU_LIMIT; i++ {
		<-c
	}

	results := "<tr><td><a href='/look/sample'>" + time.Now().Format(time.RFC1123) + "</td><td>abcDef</a></td></tr>"
	return template.HTML(results)
}

func doSearch(c chan result) {
	//r := &result{rank: 0, isLang: true, isTitle: true, isOwner: true}
	//c <- r
}
