package main

import (
	"bytes"
	"code.google.com/p/go-sqlite/go1/sqlite3"
	"encoding/base64"
	"fmt"
	"html/template"
	"io"
	"strconv"
)

const DB_NAME = "golook.db3"

/**
Given a paste struct, saves the body, date, and assigns
a random URI for association in the database.
Returns the URI as a string
*/
func SavePaste(p *Paste) string {

	pc := make(chan string)

	go func() {
		conn := DbPool.GetConnection().(*sqlite3.Conn)

		defer DbPool.ReleaseConnection(conn)

		body64 := base64.StdEncoding.EncodeToString([]byte(p.Body))

		urid := NewRS(RandPool).NewRandomString(5) /* The urid is a random 5 character URL path e.g. go.look/paste/aFdcE */

		args := sqlite3.NamedArgs{"$body": body64, "$date": p.Date, "$urid": urid, "$title": p.Title, "$lang": p.Lang}

		err := conn.Exec(`INSERT INTO pastes(body, date, urid, title, lang) VALUES($body, $date, $urid, $title, $lang)`, args)
		if err != nil {
			fmt.Printf("Error while Inserting: %s\n", err)
		}

		pc <- urid
	}()

	return <-pc
}

/**
Given a URI string, retrieve the Paste struct, save in the db
*/
func GetPaste(urid string) *Paste {
	pc := make(chan *Paste)

	go func() {
		var paste Paste
		conn := DbPool.GetConnection().(*sqlite3.Conn)

		defer DbPool.ReleaseConnection(conn)

		s, err := conn.Query("SELECT * FROM pastes WHERE urid = '" + urid + "'")
		if err != nil {
			if err == io.EOF {
				paste.Body = "Oops this page no longer exists or maybe... it never did :)"
			} else {
				fmt.Printf("Error executing query: %s\n", err.Error())
				//TODO Throw 500 error
			}
		} else {
			s.Scan(&paste.Title, &paste.Body, &paste.Date, &paste.URID, &paste.Lang, &paste.Owner)
			decoded, _ := base64.StdEncoding.DecodeString(paste.Body)
			paste.Body = string(decoded[:])
			s.Close() /* Don't forget to close our statement */
		}
		pc <- &paste
	}()
	return <-pc
}

/**
Get the XX most recent pastes in HTML table form
This is too rigid to be of practical use
*/
func GetRecent() template.HTML {
	pc := make(chan template.HTML)

	go func() {
		conn := DbPool.GetConnection().(*sqlite3.Conn)

		defer DbPool.ReleaseConnection(conn)

		sql := "SELECT date, urid FROM pastes ORDER BY rowid DESC"

		var buffer bytes.Buffer
		for s, err := conn.Query(sql); err == nil; err = s.Next() {
			var paste Paste
			s.Scan(&paste.Date, &paste.URID)
			buffer.WriteString("<tr class='clickableRow' href='/look/" + paste.URID + "'><td>" + paste.Title + "</td><td>" + paste.Lang + "</td><td>" + paste.Owner + "</td><td>" + paste.Date + "</td><td>" + paste.URID + "</a></td></tr>")
			//s.Close() //DO NOT CLOSE, will stop the iteration!! DUH!
		}

		pc <- template.HTML(buffer.String())
	}()

	return <-pc
}

func RemovePastes() {

	go func() {
		conn := DbPool.GetConnection().(*sqlite3.Conn)

		defer DbPool.ReleaseConnection(conn)
		sql := "SELECT rowid, date FROM pastes"
		for s, err := conn.Query(sql); err == nil; err = s.Next() {
			var rowid int
			s.Scan(&rowid) // Assigns 1st column to rowid
			fmt.Println(strconv.Itoa(rowid))
			sql = "DELETE FROM pastes where rowid = " + strconv.Itoa(rowid)
			conn.Exec(sql)
			//s.Close()
		}
	}()
}

/// Retrive the last insert or most recent insert
/// as the URI string
func GetLastPaste() string {
	pc := make(chan string)

	go func() {
		var urid string
		conn := DbPool.GetConnection().(*sqlite3.Conn)

		defer DbPool.ReleaseConnection(conn)
		sql := "SELECT urid FROM pastes"
		for s, err := conn.Query(sql); err == nil; err = s.Next() {
			s.Scan(&urid)
			s.Close() /* Stop at the first */
		}

		if urid == "" {
			urid = "0xDEADBEEF"
			//TODO do something useful?
		}

		pc <- urid
	}()

	return <-pc

}

func InitPoolConnections() (interface{}, error) {
	conn, err := sqlite3.Open(DB_NAME)
	if err != nil {
		fmt.Printf("Error opening db: %s\n", err.Error())
		return nil, err
	}

	conn.Exec(`CREATE TABLE language(id INTEGER PRIMARY KEY AUTOINCREMENT, shortName TEXT NOT NULL, displayName TEXT, syntaxFile TEXT)`)
	conn.Exec(`CREATE TABLE users   (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL, email TEXT, token TEXT, picture TEXT)`)
	conn.Exec(`CREATE TABLE pastes (id INTEGER PRIMARY KEY AUTOINCREMENT,body BLOB,date TEXT,urid TEXT,title TEXT,lang TEXT,owner TEXT, 
				FOREIGN KEY(lang) REFERENCES language(shortName),
				FOREIGN KEY(owner) REFERENCES users(nickName))`)

	return conn, nil
}
