package main

import (
	"code.google.com/p/goauth2/oauth"
	"html/template"
	"net/http"
)

const profileInfoURL = "https://www.googleapis.com/oauth2/v1/userinfo?alt=json"

var notAuthenticatedTemplate = template.Must(template.New("").Parse(`
<html><body>
You have currently not given permissions to access your data. Please authenticate this app with the Google OAuth provider.
<form action="/authorize" method="POST"><input type="submit" value="Ok, authorize this app with my id"/></form>
</body></html>
`))

var userInfoTemplate = template.Must(template.New("").Parse(`
<html><body>
This app is now authenticated to access your Google user info.  Your details are:<br />
{{.}}
</body></html>
`))

// variables used during oauth protocol flow of authentication
var (
	code  = ""
	token = ""
)

var oauthCfg = &oauth.Config{
	ClientId:     "244946461111.apps.googleusercontent.com",
	ClientSecret: "ABiWiWLQNpyrE_O2rPUQxv5B",
	AuthURL:      "https://accounts.google.com/o/oauth2/auth",
	TokenURL:     "https://accounts.google.com/o/oauth2/token",
	RedirectURL:  "http://localhost:8090/oauth2callback",
	Scope:        "https://www.googleapis.com/auth/userinfo.profile",
	TokenCache:   oauth.CacheFile(".cache"),
}

func HandleAuthorize(w http.ResponseWriter, r *http.Request) {
	url := oauthCfg.AuthCodeURL("")
	http.Redirect(w, r, url, http.StatusFound)
}

func HandleOAuth2Callback(w http.ResponseWriter, r *http.Request) {
	code := r.FormValue("code")

	t := &oauth.Transport{Config: oauthCfg}
	t.Exchange(code)
	resp, _ := t.Client().Get(profileInfoURL)

	buf := make([]byte, 1024)
	resp.Body.Read(buf)
	userInfoTemplate.Execute(w, string(buf))
}
