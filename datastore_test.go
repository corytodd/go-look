package main

import (
	"fmt"
	"testing"
	"time"
)

func TestDatastore(t *testing.T) {
	/**
	Initialize database pool for DB_POOL connection or die
	*/
	err := DbPool.InitPool(DB_POOL, InitPoolConnections)
	if err != nil {
		fmt.Println("Error opening db" + err.Error() + ". Abort!")
		t.Errorf("Failed to open database")
	}

	input := &Paste{Title: "Title", Body: "Body", Date: time.Now().Format(time.RFC850), Lang: "Go", Owner: "Cory"}
	urid := SavePaste(input)
	input.URID = urid

	output := GetPaste(urid)

	recent := GetLastPaste()

	fmt.Printf("Input: %+v\n", input)
	fmt.Printf("Output: %+v\n", output)
	fmt.Printf("Recent: %+v\n", recent)

}
