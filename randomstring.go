package main

import (
	"math/rand"
	"time"
)

const RandPool = "abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ"

type RS struct {
	pool string
	rg   *rand.Rand
	Used map[string]int
}

func NewRS(pool string) *RS {
	return &RS{
		pool,
		rand.New(rand.NewSource(time.Now().UnixNano())),
		make(map[string]int),
	}
}

func (rs *RS) NewRandomString(length int) (r string) {
	if length < 1 {
		return
	}
	b := make([]byte, length)
	for retries := 0; ; retries++ {
		for i, _ := range b {
			b[i] = rs.pool[rs.rg.Intn(len(rs.pool))]
		}
		r = string(b)
		_, Used := rs.Used[r]
		if !Used {
			break
		}
		if retries == 3 {
			return ""
		}
	}
	rs.Used[r] = 0
	return
}
